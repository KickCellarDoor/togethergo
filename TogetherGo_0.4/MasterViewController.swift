//
//  MasterViewController.swift
//  TogetherGo_0.3
//
//  Created by cxxnzb on 2017/1/10.
//  Copyright © 2017年 KickCellarDoor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ESPullToRefresh
import ChameleonFramework

class MasterViewController: UITableViewController {
    
    @IBOutlet var table: UITableView!
    var detailViewController: DetailViewController? = nil
    var objects = [Any]()
    var infos = [JSON]()
    
    override func viewDidLoad() {
       // self.setStatusBarStyle(UIStatusBarStyleContrast)
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //self.editButtonItem.title = "编辑"
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem
      //  self.view.backgroundColor = UIColor(red:0x2c / 255 , green: 0x3e / 255 , blue: 0x50 / 255,alpha: 1.0)
        self.title = "目前发布"
      //  self.view.subviews[0].backgroundColor = UIColor(red:0x2c / 255 , green: 0x3e / 255 , blue: 0x50 / 255,alpha: 1.0)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        addButton.title = "发布"
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        self.table.es_addPullToRefresh {
            [weak self] in
            self?.UpdateIP()
            self?.tableView.es_stopPullToRefresh()
        }
        
        DispatchQueue.global().async {
            
            DispatchQueue.main.async {
                
                self.UpdateIP()
                
            } 
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func insertNewObject(_ sender: Any) {
        performSegue(withIdentifier: "post", sender: self)
        
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = infos[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let object = objects[indexPath.row] as! String
        cell.textLabel!.text = object
     //   cell.textLabel?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)

    //    cell.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            infos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func UpdateIP(){
        self.objects.removeAll()
        self.infos.removeAll()
        let target = "http://127.0.0.1:3000/data"
        //Alamofire.request(target,method: .get).responseJSON {
        Alamofire.request(target, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                var i = 0
                for (_,subJson):(String, JSON) in json {
                    self.objects.insert(subJson["description"].stringValue, at: i)
                    self.infos.insert(subJson, at: i)
                    i += 1
                }
            case .failure(let error):
                print(error)
            }
            
            /*
             guard let json = response.result.value, response.result.error == nil else {
             return
             }
             guard let desJsons = (json as AnyObject).value(forKey: "description") as? [String] else { print("no");return }
             desJsons.forEach {des in
             self.objects.insert(des, at: 0)
             }
             
             */
            print("hi")
            //self.objects.insert("hi", at: 0)
            self.tableView.reloadData()
        }
    }
}



