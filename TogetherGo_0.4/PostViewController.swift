//
//  PostViewController.swift
//  TogetherGo_0.3
//
//  Created by cxxnzb on 2017/1/11.
//  Copyright © 2017年 KickCellarDoor. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EventKit

class PostViewController : UIViewController,UITextFieldDelegate{
    var des : UITextField? = nil
    var time : UIDatePicker? = nil
    var location : UITextField? = nil
    var no : UITextField? = nil
    var postButton : UIButton? = nil
    
    override func viewDidLoad() {
        resharpe()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func resharpe(){
        //self.view.backgroundColor = UIColor(red :0x34 / 255,green:0x45/255,blue: 0x5e/255,alpha : 0.5)
        self.view.backgroundColor = UIColor.flatGray()
        self.des = UITextField()
        self.des?.delegate = self
        self.des?.contentVerticalAlignment = .top
        self.des?.borderStyle = .roundedRect
        //self.des?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
        //self.des?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.des?.placeholder = "事件描述"
        self.des?.layer.borderWidth = 1.0
        self.des?.layer.cornerRadius = 5
        self.view.addSubview(self.des!)
        
        self.des?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(80)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(77)
        }
        
        self.time = UIDatePicker()
        //self.time?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
        //self.time?.backgroundColor = UIColor.clear
        
        self.view.addSubview(self.time!)
        self.time?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.des?.snp_bottom)!).offset(10)
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.lessThanOrEqualTo(200)
            
        }
        /*
        self.time = UITextField()
        self.time?.placeholder = "执行时间"
        self.time?.layer.borderWidth = 1.0
        self.view.addSubview(self.time!)
        
        self.time?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.des?.snp_bottom)!).offset(10)
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.equalTo(150)
        }
        */
        
        
        self.location = UITextField()
        self.location?.contentVerticalAlignment = .top
        //self.location?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
        //self.location?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.location?.placeholder = "执行地点"
        self.location?.layer.borderWidth = 1.0
        self.location?.layer.cornerRadius = 5
        self.view.addSubview(self.location!)
        
        self.location?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.time?.snp_bottom)!).offset(10)
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.equalTo(77)
        }
        
        self.no = UITextField()
        
        self.no?.placeholder = "学号"
        self.no?.layer.borderWidth = 1.0
       // self.no?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
       // self.no?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.no?.layer.cornerRadius = 5
        self.view.addSubview(self.no!)
        
        self.no?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.location?.snp_bottom)!).offset(10)
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.equalTo(50)
        }
        
        self.postButton = UIButton.init(type: UIButtonType.system)
       // self.postButton?.setTitleColor(UIColor.white, for: UIControlState.normal)
       // self.postButton?.setTitleShadowColor(UIColor.darkGray, for: UIControlState.normal)
        //self.postButton?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
       // self.postButton?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.postButton?.layer.cornerRadius = 5
        self.postButton?.setTitle("Post", for: UIControlState.normal)
        self.view.addSubview(self.postButton!)
        
        self.postButton?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.no?.snp_bottom)!).offset(5)
            make.left.equalTo(self.view).offset(100)
            make.right.equalTo(self.view).offset(-100)
            make.height.equalTo(30)
        }
        
        self.postButton?.addTarget(self, action: Selector("tapped"), for: UIControlEvents.touchUpInside)
    
        
        /*
        self.des = UIView()
        self.des?.layer.borderWidth = 1.0
        self.des?.layer.borderColor = UIColor.lightGray.cgColor
        self.des?.backgroundColor = UIColor.brown
        self.des?.layer.cornerRadius = 5
        
        self.view.addSubview(self.des!)
        self.des?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(self.view).offset(80)
            make.right.equalTo(self.view).offset(-15)
            make.left.equalTo(self.view).offset(15)
            make.height.equalTo(180)
        }
        
        let label1 = UILabel()
        self.des?.addSubview(label1)
        label1.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(label1.superview!).offset(5)
            make.left.equalTo(label1.superview!).offset(5)
            make.right.equalTo(label1.superview!).offset(-5)
            make.bottom.equalTo(label1.superview!).offset(-5)
        }
        self.des_content = label1
        */
    }
    
    func tapped(){
        if(self.des?.text?.isEmpty != true){
            let post_des = self.des?.text
            //let post_time = self.time?.text
            let post_time = self.time?.date.description
            let post_location = self.location?.text
            let post_no = self.no?.text
            let para : Parameters = [
                "description": post_des! as String,
                "time":post_time! as String,
                "location":post_location! as String,
                "member":[post_no!] as [String]
            ]
            Alamofire.request("http://127.0.0.1:3000/data", method: .post, parameters: para,encoding:JSONEncoding.default)
            
            let eventStore = EKEventStore()
            eventStore.requestAccess(to: .event) { (result, erro) in }
            let event = EKEvent(eventStore: eventStore)
            // 事件名称
            event.title = "提醒" // 事件名称
            event.notes = (self.des?.text)! as String // 事件备注
            
            // 事件地点
            event.location = (self.location?.text)! as String
            
            // 这个设置为true 开始结束时间就不会显示
            event.isAllDay = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZ"
            // 开始时间
            event.startDate = (self.time?.date.addingTimeInterval(-1200))!
            // 结束时间
            event.endDate = (self.time?.date)!
            
            event.addAlarm(EKAlarm(relativeOffset: -60*15)) // 设置提醒
            
            event.calendar = eventStore.defaultCalendarForNewEvents
            
            do {
                let _ = try eventStore.save(event, span: .thisEvent)
                
            } catch {
                print(error)
            }
        }
    }
}
