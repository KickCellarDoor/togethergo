//
//  DetailViewController.swift
//  TogetherGo_0.3
//
//  Created by cxxnzb on 2017/1/10.
//  Copyright © 2017年 KickCellarDoor. All rights reserved.
//

import UIKit
import SwiftyJSON
import SnapKit
import Alamofire
import EventKit

class DetailViewController: UIViewController {
    var id : Int = -1
    var des : UIView? = nil
    var time: UIView? = nil
    var location: UIView? = nil
    var member: UIView? = nil
    
    var des_content : UILabel? = nil
    var time_content : UILabel? = nil
    var location_content: UILabel? = nil
    var member_content: UILabel? = nil
    
    var join : UITextField? = nil
    var join_button : UIButton? = nil
    
    var nos : [String]? = []
    
    func configureView() {
         //Update the user interface for the detail item.
         if let detail = self.detailItem {
            let des = detail["description"].stringValue
            let time = detail["time"].stringValue
            let location = detail["location"].stringValue
            let member = detail["member"].array
            
            self.id = detail["id"].intValue
            self.des_content?.text = des
            self.time_content?.text = time
            self.location_content?.text = location
            
            self.member_content?.text = ""
            self.nos = []
            for m  in member!{
                self.member_content?.text?.append(m.stringValue + ",")
                
                self.nos!.append(m.stringValue)
            }
        }
        self.reloadInputViews()
        
    }
    
    func resharpe(){
        self.view.backgroundColor = UIColor.flatGray()
        //des
        self.des = UIView()
        self.des?.layer.borderWidth = 1.0
    //    self.des?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
    //    self.des?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.des?.layer.cornerRadius = 5
        
        self.view.addSubview(self.des!)
        self.des?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(self.view).offset(80)
            make.right.equalTo(self.view).offset(-15)
            make.left.equalTo(self.view).offset(15)
            make.height.equalTo(88)
        }
        
        let label1 = UILabel()
        label1.adjustsFontSizeToFitWidth = true
        label1.textAlignment = .center
        self.des?.addSubview(label1)
        label1.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(label1.superview!).offset(5)
            make.left.equalTo(label1.superview!).offset(5)
            make.right.equalTo(label1.superview!).offset(-5)
            make.bottom.equalTo(label1.superview!).offset(-5)
        }
        self.des_content = label1
        
        //time
        self.time = UIView()
        self.time?.layer.borderWidth = 1.0
   //     self.time?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
 //       self.time?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.time?.layer.cornerRadius = 5
        
        self.view.addSubview(self.time!)
        self.time?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.des?.snp_bottom)!).offset(10)
            make.right.equalTo(self.view).offset(-15)
            make.left.equalTo(self.view).offset(15)
            make.height.equalTo(88)
        }
        
        let label2 = UILabel()
        label2.adjustsFontSizeToFitWidth = true
        label2.textAlignment = .center
        self.time?.addSubview(label2)
        label2.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(label2.superview!).offset(5)
            make.left.equalTo(label2.superview!).offset(5)
            make.right.equalTo(label2.superview!).offset(-5)
            make.bottom.equalTo(label2.superview!).offset(-5)
        }
        
        self.time_content = label2
 
        //location
        self.location = UIView()
        self.location?.layer.borderWidth = 1.0
        //self.location?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
       // self.location?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.location?.layer.cornerRadius = 5
        
        self.view.addSubview(self.location!)
        self.location?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.time?.snp_bottom)!).offset(10)
            make.right.equalTo(self.view).offset(-15)
            make.left.equalTo(self.view).offset(15)
            make.height.equalTo(88)
        }
        
        let label3 = UILabel()
        label3.adjustsFontSizeToFitWidth = true
        label3.textAlignment = .center
        self.location?.addSubview(label3)
        label3.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(label3.superview!).offset(5)
            make.left.equalTo(label3.superview!).offset(5)
            make.right.equalTo(label3.superview!).offset(-5)
            make.bottom.equalTo(label3.superview!).offset(-5)
        }
        
        self.location_content = label3
        
        //member
        self.member = UIView()
        self.member?.layer.borderWidth = 1.0
       // self.member?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
        //self.member?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.member?.layer.cornerRadius = 5
        
        self.view.addSubview(self.member!)
        self.member?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.location?.snp_bottom)!).offset(10)
            make.right.equalTo(self.view).offset(-15)
            make.left.equalTo(self.view).offset(15)
            make.height.equalTo(88)
        }
        
        let label4 = UILabel()
        label4.adjustsFontSizeToFitWidth = true
        label4.textAlignment = .center
        self.member?.addSubview(label4)
        label4.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo(label4.superview!).offset(5)
            make.left.equalTo(label4.superview!).offset(5)
            make.right.equalTo(label4.superview!).offset(-5)
            make.bottom.equalTo(label4.superview!).offset(-5)
        }
        
        self.member_content = label4
        
        //join
        self.join = UITextField()
        //self.join?.backgroundColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5)
        self.join?.placeholder = "学号："
        self.join?.layer.borderColor = UIColor(red :0xbd / 255,green:0xc3/255,blue: 0xc7/255,alpha : 0.5).cgColor
        self.join?.layer.cornerRadius = 5
        self.join?.layer.borderWidth = 1.0
        self.join?.adjustsFontSizeToFitWidth = true
        self.join?.minimumFontSize = 4
        self.view.addSubview(self.join!)
        self.join?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.member?.snp_bottom)!).offset(10)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(30)
        }
        
        self.join_button = UIButton.init(type: UIButtonType.system)
       // self.join_button?.setTitleColor(UIColor.white, for: UIControlState.normal)
        //self.join_button?.setTitleShadowColor(UIColor.darkGray, for: UIControlState.normal)
        //self.join_button?.backgroundColor = UIColor.clear
        self.join_button?.layer.cornerRadius = 5
        self.join_button?.setTitle("Join", for: UIControlState.normal)
        
        self.view.addSubview(self.join_button!)
        self.join_button?.snp.makeConstraints{
            (make) -> Void in
            make.top.equalTo((self.join?.snp_bottom)!).offset(8)
            make.left.equalTo(self.view).offset(100)
            make.right.equalTo(self.view).offset(-100)
            make.height.equalTo(30)
        }
        
        self.join_button?.addTarget(self, action: Selector("tapped"), for: UIControlEvents.touchUpInside)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
        DispatchQueue.global().async {
            
            DispatchQueue.main.async {
                self.resharpe()

                self.configureView()
                
            } 
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var detailItem: JSON? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func tapped(){
        if(self.join?.text?.isEmpty == false){
            let url = "http:127.0.0.1:3000/data/" + self.id.description
            let temp = self.nos
            //print(self.nos?.description)
            self.nos?.append((self.join?.text)!)
            //print(self.member_content?.text)
            //print(self.nos?.count)
            //print(self.nos?.description)
            let para : [String:Any] = [
                "member" : self.nos ?? "error"
            ]
           
            Alamofire.request(url,method:.patch,parameters: para,encoding:JSONEncoding.default)
            
            let eventStore = EKEventStore()
            eventStore.requestAccess(to: .event) { (result, erro) in }
            let event = EKEvent(eventStore: eventStore)
            // 事件名称
            event.title = "提醒" // 事件名称
            event.notes = (self.des_content?.text)! as String // 事件备注
            
            // 事件地点
            event.location = (self.location_content?.text)! as String
            
            // 这个设置为true 开始结束时间就不会显示
            event.isAllDay = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZ"
            let date = dateFormatter.date(from: (self.time_content?.text)!)?.addingTimeInterval(1200)
            // 开始时间
            event.startDate = date!
            // 结束时间
            event.endDate = date!.addingTimeInterval(1200)
            
            event.addAlarm(EKAlarm(relativeOffset: -60*15)) // 设置提醒
            
            event.calendar = eventStore.defaultCalendarForNewEvents
            
            do {
                let _ = try eventStore.save(event, span: .thisEvent)
                
            } catch {
                print(error)
            }
            
            //Alamofire.request(url, HTTPMethod.patch, parameters: para)
            self.nos = temp
        }
    }
}



